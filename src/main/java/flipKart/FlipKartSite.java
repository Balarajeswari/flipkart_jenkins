package flipKart;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class FlipKartSite {
	@Test
	public void flip() throws InterruptedException
	{
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.flipkart.com/");
		driver.findElementByXPath("//button[@class='_2AkmmA _29YdH8']").click();

		Actions action = new Actions(driver);
		action.moveToElement(driver.findElementByXPath("//span[@class='_1QZ6fC _3Lgyp8']")).perform();
		Thread.sleep(300);
		driver.findElementByLinkText("Mi").click();
		Thread.sleep(300);
		String title = driver.getTitle();
		System.out.println(title);
		if(title.contains("Mi Mobile Phones"))
		{
			System.out.println("Title verified successfully");
		}
		else
		{
			System.out.println("Title not verified successfully");
		}
		Thread.sleep(3000);
		//driver.findElementByXPath("//div[@class='_3ywJNQ']/child::div[4]").click();
		driver.findElementByXPath("//div[@class='_1xHtJz'][3]").click();
		Thread.sleep(3000);
		

//		List<String> sting =  new ArrayList<>();		
//		for (WebElement eachProduct : productName) {
//			sting.add(eachProduct.getText());			
//			Thread.sleep(250);
//		}
//		
//		List<WebElement> productPrice = driver.findElementsByXPath("//div[@class='_1vC4OE _2rQ-NK']");
//		List<String> price =  new ArrayList<>();		
//		for (WebElement eachP : productPrice) {
//			price.add(eachP.getText());			
//			Thread.sleep(250);
//		}
//		
//		
//		for (int i = 0; i < productName.size()-1; i++) {
//			
//			System.out.println(sting.get(i)+"----->"+price.get(i));
//		}		
		Thread.sleep(3000);
		List<WebElement> productName = driver.findElementsByClassName("_3wU53n");
		List<WebElement> productPrice = driver.findElementsByXPath("//div[@class='_1vC4OE _2rQ-NK']");
		for(int i=0; i<productName.size(); i++ )
		{
		System.out.println( productName.get(i).getText()+" "+ productPrice.get(i).getText());
		}

		
		driver.findElementByClassName("_3wU53n").click();
		String text = driver.findElementByClassName("_3wU53n").getText();
		Set<String> allwindows = driver.getWindowHandles();
		List<String> lst = new ArrayList<>();
		lst.addAll(allwindows);
		driver.switchTo().window(lst.get(1));
		Thread.sleep(300);
		String title2 = driver.getTitle();
		System.out.println(title2);
		if(title2.contains(text))
		{
			System.out.println("Title verified successfully");
		}
		else
		{
			System.out.println("Title not verified successfully");
		}
		Thread.sleep(500);
		String ratings = driver.findElementByXPath("//div[@class='col-12-12']/child::span").getText();
		String reviews = driver.findElementByXPath("//div[@class='col-12-12']/following::span[1]").getText();
		String[] split = ratings.split("[ ]");
		String[] split2 = reviews.split("[ ]");
		System.out.println("No of Ratings: "+split[0]+" No of Reviews: "+split2[0]);

	}
}
