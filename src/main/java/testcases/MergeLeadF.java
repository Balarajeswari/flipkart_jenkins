package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class MergeLeadF extends ProjectSpecificMethod{
	
	@BeforeClass(groups  = "common")
	public void setData() {
		testcaseName = "TC01_CreateLead";
		testDesc = "Create a Lead with mandate values";
		author = "Bala";
		category  = "Smoke";
		fileName = "MergeLead";
	}
	@Test(dataProvider="fetchData")
	//@Test(timeOut=20000)//3rd
	//@Test(enabled = false)
	//@Test(groups  = "regression", dependsOnGroups = "sanity")//group1
	public void MergeLead(String fromlead, String tolead) throws InterruptedException 
	{
		WebElement crmsfalink = locateElement("link", "CRM/SFA");
		click(crmsfalink);
		WebElement leadtab = locateElement("link", "Leads");
		click(leadtab);
		WebElement Mergeleadtab = locateElement("link", "Merge Leads");
		click(Mergeleadtab);
		WebElement FromText = locateElement("xpath", "//input[@name='partyIdFrom']/following::a");
		click(FromText);
		switchToWindow(1);
		
		WebElement leadId = locateElement("name", "id");
		type(leadId,fromlead);
		WebElement findLeadsButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLeadsButton);
		Thread.sleep(3000);
		WebElement firstLink = locateElement("xpath","//a[@class='linktext']");
		firstLink.click();
		//click(firstLink);
		Thread.sleep(300);
		switchToWindow(0);
		
		WebElement toText = locateElement("xpath", "//input[@name='partyIdTo']/following::a[1]");
		click(toText);
		switchToWindow(1);
		WebElement toleadId = locateElement("name", "id");
		type(toleadId,tolead);
		WebElement findLeadsButton1 = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLeadsButton1);
		Thread.sleep(3000);
		WebElement tofirstLink = locateElement("xpath","//a[@class='linktext']");
		tofirstLink.click();
		switchToWindow(0);
		
		WebElement mergeButton = locateElement("xpath", "//input[@name='partyIdTo']/following::a[2]");
		mergeButton.click();
		Thread.sleep(3000);
		acceptAlert();
		WebElement findLeadsTab = locateElement("link","Find Leads");
		click(findLeadsTab);
		WebElement findleadId = locateElement("name", "id");
		type(findleadId,fromlead);
		WebElement findLeadsButton2 = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLeadsButton2);
		WebElement error = locateElement("xpath", "//div[@class='x-paging-info']");
		error.getText();
		verifyPartialText(error,"No");
	}
	/*@DataProvider(name = "fetchData")
	public String[][] getData(){
		String[][] data = new String[2][3]; 
		data[0][0] = "10234";
		data[0][1] = "10234";
	
		return data;
				
	}*/
	
	
	
}
