package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DuplicateLeadF extends ProjectSpecificMethod{
	@BeforeClass (groups  = "common")
	public void setData() {
		testcaseName = "TC01_DuplicateLead";
		testDesc = "Duplicate a Lead with mandate values";
		author = "Bala";
		category  = "Smoke";
	}
	
	@Test(dataProvider="fetchData")
	//@Test(enabled= false)//3rd
	public void duplicateLead(String email) 
	{
		WebElement crmsfalink = locateElement("link", "CRM/SFA");
		click(crmsfalink);
		WebElement leadtab = locateElement("link", "Leads");
		click(leadtab);
		WebElement findleadstab = locateElement("link", "Find Leads");
		click(findleadstab);
		WebElement emailTab = locateElement("xpath", "//span[text()='Email']");
		click(emailTab);
		WebElement emailAdd = locateElement("name","emailAddress");
		type(emailAdd, email);
		WebElement findleadsbutton = locateElement("xpath","//button[text()='Find Leads']");
		click(findleadsbutton);
		WebElement firstlead = locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/child::a");
		click(firstlead);
		WebElement firstname = locateElement("id","viewLead_firstName_sp");
		String text = firstname.getText();
		WebElement duplicatetab = locateElement("link","Duplicate Lead");
		click(duplicatetab);
		System.out.println(verifyTitle("Duplicate Lead | opentaps CRM"));
		WebElement Createleadbutton = locateElement("name","submitButton");
		click(Createleadbutton);
		WebElement duplicatefirstname = locateElement("id","viewLead_firstName_sp");
		verifyExactText(duplicatefirstname, text);
	}
	
//	@DataProvider(name = "fetchData")
//	public String[][] getData(){
//		String[][] data = new String[1][1]; 
//		data[0][0] = "bala@gmail.com";
//		return data;
//	}
}
