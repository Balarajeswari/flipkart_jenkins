package testcases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import excel.LearnExcel;
import wdMethods.SeMethods;

public class ProjectSpecificMethod extends SeMethods{
	
	@Parameters({"browser","url","username","password"})
@BeforeMethod (groups  = "common")
	public void doLogin(String browser,String url,String username,String password)
	{
		startApp(browser,url);
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("id", "password");
		type(elePassword, password);
		
		/*startApp("chrome","http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id", "password");
		type(elePassword, "crmsfa");*/
		WebElement elelogin = locateElement("classname","decorativeSubmit");
		click(elelogin);
	}
@AfterMethod (groups  = "common")
public void closeApp()
{
	closeBrowser();
}

@DataProvider(name = "fetchData")
public static Object[][] getData() throws IOException{
	 return LearnExcel.excelData(fileName);
}
}


