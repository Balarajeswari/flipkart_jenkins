package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class DeleteLeadF extends ProjectSpecificMethod{
	@BeforeClass (groups  = "common")
	public void setData() {
		testcaseName = "TC01_DeleteLead";
		testDesc = "Delete a Lead with mandate values";
		author = "Bala";
		category  = "Smoke";
		fileName = "DeleteLead";
	}
	@Test(dataProvider="fetchData")
	//@Test(dependsOnMethods = "testcases.CreateLeadF.login")//3rd
	public void deleteLead(String num) throws InterruptedException{
	
		WebElement crmsfalink = locateElement("link", "CRM/SFA");
		click(crmsfalink);
		WebElement leadtab = locateElement("link", "Leads");
		click(leadtab);
		WebElement findleadstab = locateElement("link", "Find Leads");
		click(findleadstab);
		WebElement phoneTab = locateElement("xpath", "//span[text()='Phone']");
		click(phoneTab);
		WebElement phoneNum = locateElement("name","phoneNumber");
		type(phoneNum,num);
		WebElement findleadsbutton = locateElement("xpath","//button[text()='Find Leads']");
		click(findleadsbutton);
		WebElement firstlead = locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/child::a");
		String text = firstlead.getText();
		
		click(firstlead);
		WebElement deletetab = locateElement("link","Delete");
		click(deletetab);
		Thread.sleep(300);
		WebElement findleadstab1 = locateElement("link", "Find Leads");
		click(findleadstab1);
		WebElement leadId = locateElement("name","id");
		type(leadId,text);
		WebElement findleadsbutton1 = locateElement("xpath","//button[text()='Find Leads']");
		click(findleadsbutton1);
		WebElement leadIdverify = locateElement("xpath","//div[@class='x-toolbar x-small-editor']/child::div");
		verifyPartialText(leadIdverify,"No");
		
	}
	
	
	/*@DataProvider(name = "fetchData")
	public String[][] getData(){
		String[][] data = new String[1][1]; 
		data[0][0] = " ";
		return data;
				
	}*/

}
