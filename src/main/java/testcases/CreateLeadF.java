package testcases;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class CreateLeadF extends ProjectSpecificMethod{
	@BeforeClass (groups  = "common")
	public void setData() {
		testcaseName = "TC01_CreateLead";
		testDesc = "Create a Lead with mandate values";
		author = "Bala";
		category  = "Smoke";
		fileName = "CreateLead";//for passing filename in dataprovider
	}
	
	@Test(dataProvider="fetchData")
	//@Test(invocationCount=2)//3rd
	//@Test(invocationCount = 2,timeOut = 40000)
	//@Test(groups = "smoke")//group1
	public void login(String cname, String fname, 
            String lname){
		
		WebElement crmsfalink = locateElement("link", "CRM/SFA");
		click(crmsfalink);
		WebElement leadtab = locateElement("link", "Leads");
		click(leadtab);
		WebElement createleadtab = locateElement("link", "Create Lead");
		click(createleadtab);		
		WebElement companyname = locateElement("id", "createLeadForm_companyName");
		type(companyname, cname);
		WebElement firstname = locateElement("id", "createLeadForm_firstName");
		type(firstname, fname);
		WebElement lastname = locateElement("id", "createLeadForm_lastName");
		type(lastname, lname);
		
		/*WebElement firstnamelocal = locateElement("id", "createLeadForm_firstNameLocal");
		type(firstnamelocal, "bala");
		WebElement lastnamelocal = locateElement("id", "createLeadForm_lastNameLocal");
		type(lastnamelocal, "shankar");		
		WebElement personaltitle = locateElement("id", "createLeadForm_personalTitle");
		type(personaltitle, "Hello");
		WebElement revenue = locateElement("id", "createLeadForm_annualRevenue");
		type(revenue, "1000");
		WebElement profiletitle = locateElement("id", "createLeadForm_generalProfTitle");
		type(profiletitle, "profile");
		WebElement crncy = locateElement("id", "createLeadForm_currencyUomId");
		selectDropDownUsingText(crncy, "IRR - Iranian Rial");
		WebElement source = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(source, "Employee");
		WebElement industry = locateElement("id", "createLeadForm_industryEnumId");
		selectDropDownUsingText(industry, "Computer Software");
		WebElement owner = locateElement("id", "createLeadForm_ownershipEnumId");
		selectDropDownUsingText(owner, "Sole Proprietorship");		
		WebElement cntry = locateElement("id", "createLeadForm_generalCountryGeoId");
		selectDropDownUsingText(cntry, "India");
		WebElement state = locateElement("id", "createLeadForm_generalStateProvinceGeoId");
		selectDropDownUsingText(state, "TAMILNADU");		
		WebElement siccode = locateElement("id", "createLeadForm_sicCode");
		type(siccode, "123");
		WebElement desc = locateElement("id", "createLeadForm_description");
		type(desc, "Creating profile");
		WebElement note = locateElement("id", "createLeadForm_importantNote");
		type(note, "new");
		WebElement countrycode = locateElement("id", "createLeadForm_primaryPhoneCountryCode");
		type(countrycode, "123");
		WebElement areacode = locateElement("id", "createLeadForm_primaryPhoneAreaCode");
		type(areacode, "123");
		WebElement extnsn = locateElement("id", "createLeadForm_primaryPhoneExtension");
		type(extnsn, "456");
		WebElement weburl = locateElement("id", "createLeadForm_primaryWebUrl");
		type(weburl, "www.google.com");
		WebElement deptname = locateElement("id", "createLeadForm_departmentName");
		type(deptname, "CS");		
		WebElement ticketsymbol = locateElement("id", "createLeadForm_tickerSymbol");
		type(ticketsymbol, "AB");
		WebElement noofemployee = locateElement("id", "createLeadForm_numberEmployees");
		type(noofemployee, "5");
		WebElement phonename = locateElement("id", "createLeadForm_primaryPhoneAskForName");
		type(phonename, "ABC");
		WebElement toname = locateElement("id", "createLeadForm_generalToName");
		type(toname, "ABC");
		WebElement add1 = locateElement("id", "createLeadForm_generalAddress1");
		type(add1, "ADD1");
		WebElement add2 = locateElement("id", "createLeadForm_generalAddress2");
		type(add2, "ADD2");
		WebElement city = locateElement("id", "createLeadForm_generalCity");
		type(city, "chennai");
		WebElement postalcode = locateElement("id", "createLeadForm_generalPostalCode");
		type(postalcode, "123");
		WebElement postalcodeextn = locateElement("id", "createLeadForm_generalPostalCodeExt");
		type(postalcodeextn, "456");	
		WebElement emailadd = locateElement("id", "createLeadForm_primaryEmail");
		type(emailadd, "bala@gmail.com");*/
		WebElement createlead = locateElement("name", "submitButton");
		click(createlead);
		WebElement textdisplayed = driver.findElementById("viewLead_firstName_sp");
		String text = getText(textdisplayed);
		if(text.equals(fname))
		System.out.println(text);	
		
		
	}
	
	
	/*@DataProvider(name = "fetchData")
	public String[][] getData(){
		String[][] data = new String[2][3]; 
		data[0][0] = "Accenture";
		data[0][1] = "Bala";
		data[0][2] = "shankar";
		
		data[1][0] = "Vestas";
		data[1][1] = "Prasanth";
		data[1][2] = "Shankar";
		return data;
				
	}*/
	
}









