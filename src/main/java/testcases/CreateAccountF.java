package testcases;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class CreateAccountF extends ProjectSpecificMethod {
	
	@BeforeClass
	public void setData() {
		testcaseName = "TC01_CreateAccount";
		testDesc = "Create a Account with mandate values";
		author = "Bala";
		category  = "Smoke";
	}
	@Test
	public void createAccount() {
		WebElement crmsfalink = locateElement("link", "CRM/SFA");
		click(crmsfalink);
		WebElement accounttab = locateElement("link", "Accounts");
		click(accounttab);
		WebElement createaccounttab = locateElement("link", "Create Account");
		click(createaccounttab);
		WebElement accountname = locateElement("xpath", "//td[@class='titleCell']/following::input");
		type(accountname,"Currentaccont");
		WebElement industry = locateElement("name", "industryEnumId");
		selectDropDown(industry,"text","Computer Software");
		WebElement crncy = locateElement("id", "currencyUomId");
		selectDropDown(crncy,"text","INR - Indian Rupee");
		WebElement source = locateElement("id", "dataSourceId");
		selectDropDown(source,"text","Employee");
		WebElement marketcampaign = locateElement("id", "marketingCampaignId");
		selectDropDown(marketcampaign,"text","Demo Marketing Campaign");
		WebElement phonenum = locateElement("id", "primaryPhoneNumber");
		type(phonenum,"1234");
		WebElement city = locateElement("id", "generalCity");
		type(city,"Chennai");
		WebElement email = locateElement("id", "primaryEmail");
		type(email,"bala@gmail.com");
		WebElement country = locateElement("id", "generalCountryGeoId");
		selectDropDown(country,"text","India");
		WebElement state = locateElement("id", "generalStateProvinceGeoId");
		selectDropDown(state,"text","TAMILNADU");
		WebElement createaccountbutton = locateElement("xpath", "//input[@class='smallSubmit']");
		click(createaccountbutton);
		WebElement captureAccountId = locateElement("xpath", "//td[@class='titleCell']/following::span[1]");
		String text = captureAccountId.getText();
		
		/*Pattern pattern = Pattern.compile(".^()");
		Matcher m = pattern.matcher(text);
		System.out.println(m.matches());*/
		
		
		/*String AccountIdgenerated = text.substring(text.indexOf('(')+1,text.indexOf(')'));		
		String AccountNamegenerated = text.substring(text.indexOf(0)+1,text.indexOf('('));
		String AccountNamegenerated1= AccountNamegenerated.trim();*/
		
		String array1[]= text.split("[()]");
	       for (String temp: array1){
	          System.out.println(temp);
	       }
	       String AccountNamegenerated= array1[0].trim();
		WebElement findAccountstab = locateElement("link","Find Accounts");
		click(findAccountstab);
		
		
		WebElement AccountId = locateElement("name", "id");
		//type(AccountId,AccountIdgenerated);
		type(AccountId,array1[1]);
		WebElement AccountName = locateElement("xpath", "//input[@name=\"id\"]/following::input");
		//type(AccountName,AccountNamegenerated);
		type(AccountName,AccountNamegenerated);
		
		WebElement findAccountsButton = locateElement("xpath","//button[text()='Find Accounts']");
		click(findAccountsButton);
		
		
		WebElement verifyaccountId = locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/child::a");
		WebElement verifyaccountname = locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-groupName']/child::a");
		
		//verifyExactText(verifyaccountId,AccountIdgenerated);
		//verifyPartialText(verifyaccountname,AccountNamegenerated1);
		
		verifyExactText(verifyaccountId,array1[1]);
		verifyPartialText(verifyaccountname,AccountNamegenerated);
	}
	
}
