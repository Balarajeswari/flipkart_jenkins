package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.SeMethods;


public class EditLeadF extends ProjectSpecificMethod {
	@BeforeClass (groups  = "common")
	public void setData() {
		testcaseName = "TC01_EditLead";
		testDesc = "Edit a Lead with mandate values";
		author = "Bala";
		category  = "Smoke";
	}
	
	@Test(dataProvider="fetchData")
	//@Test(dependsOnMethods = "testcases.CreateLeadF.login")//3rd
	//@Test(groups = "sanity", dependsOnGroups = "smoke")//group1
	public void editLead(String leadName,String vtitle,String compname) throws InterruptedException{
		
	WebElement crmsfalink = locateElement("link", "CRM/SFA");
	click(crmsfalink);
	WebElement leadtab = locateElement("link", "Leads");
	click(leadtab);
	WebElement findleadstab = locateElement("link", "Find Leads");
	click(findleadstab);
	type(locateElement("xpath", "//input[@name='id']/following::input"), leadName);
	WebElement findleadsbutton = locateElement("xpath", "//button[text()='Find Leads']");
	click(findleadsbutton);
	Thread.sleep(300);
	WebElement firstvalue = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/child::a");
	click(firstvalue);
	//WebElement title = locateElement("id","sectionHeaderTitle_leads");
	System.out.println(verifyTitle(vtitle));
	WebElement edit = locateElement("link", "Edit");
	click(edit);
	
	WebElement editcompname = locateElement("id", "updateLeadForm_companyName");
	editcompname.clear();
	type(editcompname, compname);
	WebElement updatebutton = locateElement("xpath", "//input[@value='Update']");
	click(updatebutton);
	WebElement updatedcompname = locateElement("id","viewLead_companyName_sp");
	verifyExactText(updatedcompname, compname);
	}
//	@DataProvider(name = "fetchData")
//	public String[][] getData(){
//		String[][] data = new String[2][3]; 
//		data[0][0] = "Bala";
//		data[0][1] = "View Lead | opentaps CRM";
//		data[0][2] = "CTS";
//		
//	
//		return data;
//				
//	}
	
	
}
