package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class MergeAccountF extends ProjectSpecificMethod{
	
	@BeforeClass
	public void setData() {
		testcaseName = "TC02_MergeAccount";
		testDesc = "Merge Account";
		author = "Bala";
		category  = "Smoke";
	}
	@Test
	public void mergeAccount() throws InterruptedException {
		WebElement crmsfalink = locateElement("link", "CRM/SFA");
		click(crmsfalink);
		WebElement accounttab = locateElement("link", "Accounts");
		click(accounttab);
		WebElement mergeaccounttab = locateElement("link", "Merge Accounts");
		click(mergeaccounttab);
		WebElement fromAccount = locateElement("xpath", "//input[@id='partyIdFrom']/following::a");
		click(fromAccount);
		switchToWindow(1);
		WebElement findAccountId = locateElement("name", "id");
		type(findAccountId,"10649");
		WebElement findAccountName = locateElement("name", "accountName");
		type(findAccountName,"balas");
		Thread.sleep(300);
		WebElement findAccount = locateElement("xpath", "//button[text()='Find Accounts']");
		click(findAccount);
		WebElement firstaccount = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/child::a");
		//click(firstaccount);
		firstaccount.click();
		switchToWindow(0);
		
		WebElement toAccount = locateElement("xpath", "//input[@id='partyIdFrom']/following::a[2]");
		click(toAccount);
		switchToWindow(1);
		WebElement findToAccountId = locateElement("name", "id");
		type(findToAccountId,"10666");
		WebElement findToAccountName = locateElement("name", "accountName");
		type(findToAccountName,"balashankar");
		WebElement findToAccount = locateElement("xpath", "//button[text()='Find Accounts']");
		click(findToAccount);
		Thread.sleep(300);
		WebElement firstToaccount = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/child::a");
		//click(firstToaccount);
		firstToaccount.click();
		switchToWindow(0);
		WebElement mergeButton = locateElement("link", "Merge");
		click(mergeButton);
		Thread.sleep(300);
		acceptAlert();
		
		WebElement findAccountstab = locateElement("link","Find Accounts");
		click(findAccountstab);
		
		
		WebElement AccountId = locateElement("name", "id");
		type(AccountId,"10649");	
		WebElement findAccountsButton = locateElement("xpath","//button[text()='Find Accounts']");
		click(findAccountsButton);
		
		
		WebElement verifymergedno = locateElement("xpath","//div[@class='x-toolbar x-small-editor']/child::div");		
		verifyPartialText(verifymergedno,"No");
		
		
		
	}

}
