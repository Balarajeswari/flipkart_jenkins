package condstatements;

import java.util.Scanner;

public class SumMultiples {

	public static void main(String[] args) {
		System.out.println("Enter number to find Sum of Multiples of 3 and 5 less than given number");
		Scanner s = new Scanner(System.in);
		int n = s.nextInt();
		int sum = 0;
		for(int i =1; i<n ; i++)
		{if(i%3==0 || i%5==0)
		{
			sum = sum +i;
		}
		}
		System.out.println("Sum is "+sum);

	}

}
