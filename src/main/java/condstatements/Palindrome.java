package condstatements;

import java.util.Scanner;

public class Palindrome {

	public static void main(String[] args) {
		System.out.println("Enter the number to check Palindrome");
		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		int c = a;
		int d = 0;
		int b;
		while(a>0)
		{
			b = a % 10;
			a = a / 10;	
			d = (d*10) + b;
		}
		if(d == c)
		{
			System.out.println("Palindrome");
		}
		else
		{
			System.out.println("Not Palindrome");
		}
	}

}
