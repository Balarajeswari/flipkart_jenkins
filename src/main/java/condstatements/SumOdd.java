package condstatements;

import java.util.Scanner;

public class SumOdd {

	public static void main(String[] args) {
		System.out.println("enter the number");
		Scanner sc=new Scanner(System.in);
		int n = sc.nextInt();
		int a;
		int b = 0;
		while(n>0)
		{
			a = n % 10;
			if(a % 2 != 0)
			b = b + a;
			n = n / 10;
		}
		System.out.println("sum of odd numbers in a given number is " +b);
		
	}

}
