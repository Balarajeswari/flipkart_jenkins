package condstatements;

import java.util.Scanner;

public class PrimeNumber {

	public static void main(String[] args) {
		System.out.println("Enter a number");
		Scanner s = new Scanner(System.in);
		int n = s.nextInt();
		int flag=0;
		for(int i=2; i<=n/2 ; i++)
		{
			if(n%i == 0)
			{
				flag = 1;
				break;
			}
		}
		
		if(n==2)
		{
			System.out.println(n + " is a Prime number");
		}
		else if(n==1)
		{
			System.out.println(n + " is a neither a Prime number nor a composite number");
		}
		
		else if(flag==0 && n!=0)
		{
			System.out.println(n + " is a Prime number");
		}
		else
		{
			System.out.println(n + " is not a Prime number");
		}

	}

}
