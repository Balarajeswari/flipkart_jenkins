package condstatements;

import java.util.Scanner;

public class Factorial {

	public static void main(String[] args) {
		System.out.println("Enter the number to find Factorial");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int s = n;
		int m = 1;
		while(n > 0)
			{m = m * n;
				n--;
			}
		System.out.println("Factorial of the " +s +" is " +m);

	}


}
