package condstatements;

import java.util.Scanner;

public class DynamicArray {

	public static void main(String[] args) {
		System.out.println("Enter the size");
		Scanner s = new Scanner(System.in);
		int size = s.nextInt();
		System.out.println("Enter "+ size +"employee ID");
		int[] empId = new int[size];
		for(int i=0; i<size; i++)
		{
			empId[i] = s.nextInt();
		}
		for(int eachId:empId)
			{
			System.out.println(eachId);
			}

	}

}
