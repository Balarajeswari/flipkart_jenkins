package condstatements;

import java.util.Scanner;

public class SwapWithoutTemp {

	public static void main(String[] args) {
		String s1,s2;
		System.out.println("Swapping withoout using third variable");
		Scanner s = new Scanner(System.in);
		s1 = s.nextLine();
		s2 = s.nextLine();
		System.out.println("Before Swapping: s1 "+s1+" s2 "+s2);
		s1 = s1+s2;
		s2 = s1.substring(0, s1.length()-s2.length());
		s1 = s1.substring(s2.length());
		System.out.println("After Swapping: s1 "+s1+" s2 "+s2);
		

	}

}
