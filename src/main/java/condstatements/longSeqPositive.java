package condstatements;

import java.util.Scanner;

public class longSeqPositive {

	public static void main(String[] args) {
		System.out.println("Long sequence positive number");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int[] a= new int[n];
		for(int i=0; i<n; i++)
		{
			a[i] = sc.nextInt();
		}
		
		int curlen = 0, curIdx = 0, maxlen = 0, maxIdx =0;
		for(int i=0;i<n;i++)
		{
			if(a[i]>0)
			{
				curlen++;
				if(curlen==1)
				{
					curIdx=i;
				}
				if(i==n-1)
				{
					if(curlen>maxlen)
					{
						maxlen=curlen;
						maxIdx=curIdx;
					}
					curlen=0;
				}
			}
			 
			else
			{
				if(curlen>maxlen)
				{
					maxlen=curlen;
					maxIdx=curIdx;
				}
				curlen=0;
			}
		}
		
		if(maxlen<0)
		{
			System.out.println("No Positive numbers");
		}
		else
		{	
			for(int i = maxIdx ; i < maxIdx+maxlen; i++)
			{	
			System.out.print(a[i]+" ");
			
			}
		}
			
	}

}
