package condstatements;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringFiveMail {

	public static void main(String[] args) {
		System.out.println("Enter the Email ID");
		String pattern = "[\\w]{8,}@[a-z]{2,}.[a-z]{2,}";
		Pattern p = Pattern.compile(pattern);
		Scanner s = new Scanner(System.in);
		String text = s.next();
		Matcher m = p.matcher(text);
System.out.println(m.matches());
	}

}
