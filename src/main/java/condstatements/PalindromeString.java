package condstatements;

import java.util.Scanner;

public class PalindromeString {

	public static void main(String[] args) {
		System.out.println("Enter the number to check Palindrome");
		Scanner sc = new Scanner(System.in);
		String a = sc.next();
		String rev = "";
		int d = a.length();
		for(int i = d - 1;i >=0 ;i--)
		{
			rev = rev + a.charAt(i);
		}
		if(rev.equals(a))
		{
			System.out.println("Palindrome");
		}
		else
		{
			System.out.println("Not Palindrome");
		}

	}

}
