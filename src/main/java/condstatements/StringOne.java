package condstatements;

public class StringOne {

	public static void main(String[] args) {
		String name = "Bala Rajeswari";
		System.out.println("To find the no of 'a' present in given name");
		char[] ch = name.toCharArray();
		int a = 0;
		for(int i=0; i<ch.length; i++)
		{
			if(ch[i] == 'a')
			{
				a = a+1;
			}
		}
		System.out.println(a);
	}

}
