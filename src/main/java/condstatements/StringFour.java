package condstatements;

public class StringFour {

	public static void main(String[] args) {
		String name = "Bala,Priyanka,Shanthi";
		System.out.println("To find the name starts with 's'");
		
		String[] sp = name.split(",");
		for(int i=0; i<sp.length; i++)
		{
			if(sp[i].startsWith("S"))
			{
				System.out.println(sp[i]);
			}
		}

	}

}
