package condstatements;

import java.util.Scanner;

public class SumNNumbers {

	public static void main(String[] args) {
		System.out.println("Enter the value for n");
		Scanner sb = new Scanner(System.in);
		int n = sb.nextInt();
		int b = n;
		int a = 0;
		while(n > 0)
		{a = a + n;
		n--;}
		System.out.println("Sum of " +b +" is " +a);
		sb.close();
	}

}
