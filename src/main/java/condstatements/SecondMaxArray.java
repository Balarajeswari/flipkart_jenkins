package condstatements;

import java.util.Scanner;

public class SecondMaxArray {

	public static void main(String[] args) {
		System.out.println("Enter the number of Elements in an Array");
		Scanner s = new Scanner(System.in);
		int n = s.nextInt();
		int[] a = new int[n];
		int temp;
		System.out.println("Enter the Elements of an Array");
		for(int i = 0; i < n; i++)
		{
		 a[i] = s.nextInt();
		}
		for(int i =0; i < n; i++)
		{for(int j=i+1; j< n; j++)
			{if(a[i]>a[j])
			{   temp = a[i];
				a[i] = a[j];
				a[j] = temp;
			}
			}
		} 
		for(int i =0; i < n; i++)
		{
		 System.out.println(a[i]);
		}
		System.out.println("Second Maximum Value is "+a[n-2]);
	}

}
