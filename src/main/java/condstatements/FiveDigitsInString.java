package condstatements;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FiveDigitsInString {

	public static void main(String[] args) {
		System.out.println("Enter String to find it has exactly 5 digits");
		Scanner sc = new Scanner(System.in);
		 String testString = sc.nextLine();
		 String myPattern = "\\d{5}";

		    Pattern validCharacterPattern = Pattern.compile(myPattern);
		    Matcher matcher = validCharacterPattern.matcher(testString);
		    boolean b = matcher.find();

		    if (b) System.out.println("True");
		    else System.out.println("False");
		
		
		/*String s = sc.nextLine();
		 int count =0;
		for (int i=0;i<s.length();i++) {
if(Character.isDigit(s.charAt(i)))
		{
		count++;
	}
	else 
	{
	count =0;
	}
		}
		
		if(count==5)
		{
			System.out.println("true");
		}
		else
		{
			System.out.println("false");
		}*/
		
	}

}
