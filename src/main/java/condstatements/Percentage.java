package condstatements;

import java.util.Scanner;

public class Percentage {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter password");
		String a = sc.next();
		Character c;
		int upperletter = 0, digit = 0, special = 0, lowerletter = 0,upperpercent,lowerpercent,digitpercent,specialpercent;
		for(int i=0; i<=a.length()- 1; i++)
		{
			c = a.charAt(i);
			if(Character.isLetter(c))
			{	if(Character.isUpperCase(c))
				{
				upperletter++;
				}
				else
				{
					lowerletter++;	
				}
			}
			else if(Character.isDigit(c))
			{
				digit++;
			}
			else 
			{	
				special++;
			}
		}
		upperpercent = (upperletter * 100) / (upperletter+lowerletter+digit+special);
		lowerpercent = (lowerletter * 100) / (upperletter+lowerletter+digit+special);
		specialpercent = (special * 100) / (upperletter+lowerletter+digit+special);
		digitpercent = (digit * 100) / (upperletter+lowerletter+digit+special);
		
		System.out.println("Number of UpperCase letters is "+upperletter+". So Percentage is "+upperpercent+"%");
		System.out.println("Number of LowerCase letters is "+lowerletter+". So Percentage is "+lowerpercent+"%");
		System.out.println("Number of Digits is "+digit+". So Percentage is "+digitpercent+"%");
		System.out.println("Number of Other Characters is "+special+". So Percentage is "+specialpercent+"%");
		
	}

}
