package condstatements;

import java.util.Scanner;

public class FloydTriangle {

public static void main(String[] args) {
System.out.println("Enter number of rows to print Floyd Triangle");
Scanner sc = new Scanner(System.in);
int n = sc.nextInt();
sc.close();
int b =1;
for(int i =1; i<=n ; i++)
{int a = 1;
	while(a<=i)
	{
		System.out.print(b);
		a++;
		b++;
	}
	System.out.println("");
}
	}

}
