package collectionsexample;

import java.util.HashSet;
import java.util.Set;

public class ArrayListDuplicate {

	public static void main(String[] args) {
		
		int[] c = {1, 3, 4, 5, 1,3};
		System.out.println(findDuplicates(c));
		

	}
	public static Set<Integer> findDuplicates(int[] input) {
		Set<Integer> duplicates = new HashSet<Integer>();
		for (int i = 0; i < input.length; i++) 
		{

			for (int j = 1; j < input.length; j++)
			{ 
				if (input[i] == input[j] && i != j)
				{ 
				 duplicates.add(input[i]);
				 break;
				 } 
			}
		}
		return duplicates;
		}

}
