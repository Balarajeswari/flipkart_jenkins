package collectionsexample;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class TeamSet {

	public static void main(String[] args) {

		Set<String> teamMembers = new TreeSet<String> ();
		teamMembers.add("Bala");
		teamMembers.add("Ramya");
		teamMembers.add("Dhivya");
		teamMembers.add("Swathy");
		teamMembers.add("Bala");
		
		for (String each : teamMembers) {
			System.out.println(each);
			
		}
		List<String> team = new ArrayList<String> ();
		team.addAll(teamMembers);
		System.out.println(team.get(team.size()-2));
		team.add("Ramya");
		team.remove(0);
		System.out.println("Names in List");
		for (String each : team) {
			System.out.println(each);
			
		}
		System.out.println(team.size());

	}

}
