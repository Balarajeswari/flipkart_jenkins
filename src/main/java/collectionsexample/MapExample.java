package collectionsexample;

import java.util.Map;
import java.util.TreeMap;

public class MapExample {

	public static void main(String[] args) {
		Map<Character, Integer> country = new TreeMap<Character, Integer>();
		String text1 = "Paypal India";
		String text = text1.replaceAll(" ", "");
		text = text.toLowerCase();
		char[] charArray = text.toCharArray();
		
		for(char eachCharacter : charArray)
		{
			if(country.containsKey(eachCharacter))
			{
				country.put(eachCharacter,country.get(eachCharacter)+1);
			}
			else
			{
				country.put(eachCharacter , 1);
			}
		}
		System.out.println(country);
	}

}
