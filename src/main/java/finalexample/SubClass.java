package finalexample;

public class SubClass extends FinalExample {
	
	public void display()
	{
		System.out.println("SubClass Overridden");	
	}
	public static void main(String[] args) {
		SubClass sc = new SubClass();
		sc.display();
		System.out.println(sc.a = 100);
	
	}
}
