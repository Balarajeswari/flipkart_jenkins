package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.util.SystemOutLogger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import reports.BasicReports;

public class SeMethods extends BasicReports implements WdMethods{
	public RemoteWebDriver driver;	
	public int i = 1;
	
	public void startApp(String browser, String url) {
		if(browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
			driver = new ChromeDriver();
		} else if(browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "./driver/geckodrivers.exe");
			driver = new FirefoxDriver();
		}
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println("The Browser "+browser+" Launched Successfully");
		takeSnap();
	}


	public WebElement locateElement(String locator, String locValue) {
		switch (locator) {
		case "id": return driver.findElementById(locValue);
		case "classname": return driver.findElementByClassName(locValue);
		case "xpath": return driver.findElementByXPath(locValue);
		case "link": return driver.findElementByLinkText(locValue);
		case "name": return driver.findElementByName(locValue);
		case "tagname": return driver.findElementByTagName(locValue);
		case "partiallinktext":return driver.findElementByPartialLinkText(locValue);
		
		}
		return null;
	}

	public WebElement locateElement(String locValue) {

		return null;
	}
	

	@Override
	public void type(WebElement ele, String data) {
		ele.sendKeys(data);
		System.out.println("The Data "+data+" Entered Successfully");
		takeSnap();
	}

	@Override
	public void click(WebElement ele) {
		ele.click();
		System.out.println("The Element "+ele+" is clicked Successfully");
		takeSnap();
	}

	@Override
	public String getText(WebElement ele) {
		return ele.getText();
	}


	public void selectDropDownUsingText(WebElement ele, String value) {
		Select droptext = new Select(ele);
		droptext.selectByVisibleText(value);


	}
	public void selectDropDown(WebElement ele, String type,String data) {
		Select droptext = new Select(ele);
		switch(type)
		{
		case "index":droptext.selectByIndex(Integer.parseInt(data));
		break;
		case "text":droptext.selectByVisibleText(data);
		break;
		case "value":droptext.selectByValue(data);
		break;
		
		}
		


	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		Select droptext = new Select(ele);
		droptext.selectByIndex(index);
	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		
		String title = driver.getTitle();
		if(title.equals(expectedTitle))
		{
		return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		String text = ele.getText();
		if(text.equals(expectedText))
				{
			System.out.println("Verified correctly");
				}
		else
		{
			System.out.println("Incorrect");
		}
		

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		String text = ele.getText();
		if(text.contains(expectedText))
				{
			System.out.println("Verified correctly");
				}
		else
		{
			System.out.println("Incorrect");
		}

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToWindow(int index) {
		Set<String> allwindows = driver.getWindowHandles();
		List<String> lst = new ArrayList<>();
		lst.addAll(allwindows);
		driver.switchTo().window(lst.get(index));
		System.out.println("Entered the window successfully");
		takeSnap();
	}

	@Override
	public void switchToFrame(WebElement ele) {
		driver.switchTo().frame(0);

	}

	@Override
	public void acceptAlert() {
		driver.switchTo().alert().accept();

	}

	@Override
	public void dismissAlert() {
		driver.switchTo().alert().dismiss();

	}

	@Override
	public String getAlertText() {
		return driver.switchTo().alert().getText();
	}


	public void takeSnap() {
		try {
			File src = driver.getScreenshotAs(OutputType.FILE);
			File desc = new File("./snaps/img"+i+".png");
			FileUtils.copyFile(src, desc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		driver.close();

	}

	@Override
	public void closeAllBrowsers() {
		driver.quit();

	}

}
