package reports;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class BasicReports {
	static ExtentHtmlReporter html;
	static ExtentReports extent;
	ExtentTest test;
	public String testcaseName, testDesc, author, category;
     public static String fileName;
	@BeforeSuite
	public void startResult()
	{
		html = new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);
		extent = new ExtentReports();
		extent.attachReporter(html);	
	}
	@BeforeMethod
	public void startTestcase(){
		test = extent.createTest(testcaseName, testDesc);
		test.assignAuthor(author);
		test.assignCategory(category);
	}
	
	public void logStep(String desc, String status) {
		if(status.equalsIgnoreCase("PASS")) {
			test.pass(desc);
		}else if(status.equalsIgnoreCase("FAIL")) {
			test.fail(desc);
		}else if(status.equalsIgnoreCase("WARN")) {
			test.warning(desc);
		}
	}
	
	
	@AfterSuite
	public void endResult() {
		extent.flush(); 
	}
	
	
	/* Hardcoded Values 
	  public void startResult() these needs to be executed once for all TC's
	{
		html = new ExtentHtmlReporter("./reports/result.html"); ->create template for Storing report details
		html.setAppendExisting(true); ->tells whether to append the report to existing report or create new
		extent = new ExtentReports(); ->inside this class we have all methods to get for report details
		extent.attachReporter(html); ->write the reports to template(temporary)	
	}
	@Test
	public void runReport() throws IOException these needs to be executed for each TC
	{
		ExtentTest test = extent.createTest("Login","Login into Leaftaps");
		
		these needs to be executed for each test steps
		test.pass("username is entered",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		test.fail("password is not entered",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		test.assignAuthor("Bala");
		test.assignCategory("Smoke");
		
		This needs to be executed only once after every TC execution
		extent.flush(); -> if once it is given only the reports will be written in the template 
		
	}*/
	
	

}
