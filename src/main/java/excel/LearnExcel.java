package excel;

import java.io.IOException;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class LearnExcel {
	
	public static Object[][] excelData(String fileName) throws IOException {
		XSSFWorkbook workBook = new XSSFWorkbook("./data/"+fileName+".xlsx");//It just point the Excel
		//XSSFSheet sheet = workBook.getSheet("CreateLead");
		XSSFSheet sheet = workBook.getSheetAt(0);
		int rowCount = sheet.getLastRowNum();
		int cellCount = sheet.getRow(0).getLastCellNum();
		
		
		Object[][] data = new Object[rowCount][cellCount];//object array create
		
		
		for (int i = 1; i <= rowCount; i++) 
		{
			XSSFRow row = sheet.getRow(i);
			for (int j = 0; j < cellCount; j++) 
			{
				XSSFCell cell = row.getCell(j);
				String cellValue = cell.getStringCellValue();
         		data[i-1][j]=cellValue;//storing value in array
				System.out.println(cellValue);
			}
		}
				/*CellType cellType = cell.getCellTypeEnum();
				if(cellType == CellType.NUMERIC)
				{ Double cellValue1 = cell.getNumericCellValue();
				int cellValue = cellValue1.intValue();
				data[i-1][j]= cellValue;//storing value in array
				System.out.println(cellValue);
				}
				else if(cellType == CellType.STRING)
				{
							String cellValue = cell.getStringCellValue();
			         		data[i-1][j]=cellValue;//storing value in array
							System.out.println(cellValue);
				}*/
				
			
		
		workBook.close();
		return data;

	}
	


	/*public static void main(String[] args) throws IOException {
		XSSFWorkbook workBook = new XSSFWorkbook("./data/CreateLead.xlsx");//It just point the Excel
		XSSFSheet sheet = workBook.getSheet("CreateLead");
		int rowCount = sheet.getLastRowNum();
		int cellCount = sheet.getRow(0).getLastCellNum();
		
		
		for (int i = 1; i <= rowCount; i++) //last index {
			XSSFRow row = sheet.getRow(i);
			
			
			for (int j = 0; j < cellCount; j++)//last index+1 {
				XSSFCell cell = row.getCell(j);
				String cellValue = cell.getStringCellValue();
				System.out.println(cellValue);
				
			}
		}

	}*/

}
