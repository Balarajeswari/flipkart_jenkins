package week4;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		
		driver.findElementByXPath("//input[@name='partyIdFrom']/following::a").click();
		Set<String> allwindows = driver.getWindowHandles();
		List<String> lst = new ArrayList<>();
		lst.addAll(allwindows);
		String a = "10097";
		driver.switchTo().window(lst.get(1));
		driver.findElementByName("id").sendKeys(a);
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//a[@class='linktext']").click();
		driver.switchTo().window(lst.get(0));
		
		driver.findElementByXPath("//input[@name='partyIdTo']/following::a[1]").click();
		Set<String> allwindows2 = driver.getWindowHandles();
		List<String> lst2 = new ArrayList<>();
		lst2.addAll(allwindows2);
		driver.switchTo().window(lst2.get(1));
		driver.findElementByName("id").sendKeys("10105");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(3000);
		driver.findElementByXPath("//a[@class='linktext']").click();
		driver.switchTo().window(lst.get(0));
		
		driver.findElementByXPath("//input[@name='partyIdTo']/following::a[2]").click();
		driver.switchTo().alert().accept();
		//click Find Leads Tab
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByName("id").sendKeys(a);
		//click Find Leads Button
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		//String error = driver.findElementXPath("//span[@class='ytb-text']").getText();
		String error = driver.findElementByXPath("//div[@class='x-paging-info']").getText();
		System.out.println(error);
		if(error.contains("No record"))
		{
			System.out.println("True");
		}
		else
		{
			System.out.println("False");
		}
		driver.close();

	}

}
