package week4;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateLead {
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		String a = "Tom";
		driver.manage().window().maximize();
		
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Create Lead").click();
	
		driver.findElementById("createLeadForm_companyName").sendKeys("Accenture");
		driver.findElementById("createLeadForm_firstName").sendKeys(a);
		driver.findElementById("createLeadForm_lastName").sendKeys("Jerry");
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Tom");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Jerry");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("Hello");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("1000");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Profile");
		WebElement source = driver.findElementById("createLeadForm_dataSourceId");
		Select s = new Select(source);
		s.selectByVisibleText("Employee");
		WebElement industry = driver.findElementById("createLeadForm_industryEnumId");
		Select i = new Select(industry);
		i.selectByVisibleText("Computer Software");
		WebElement owner = driver.findElementById("createLeadForm_ownershipEnumId");
		Select o = new Select(owner);
		o.selectByVisibleText("Sole Proprietorship");
		driver.findElementById("createLeadForm_sicCode").sendKeys("123");
		driver.findElementById("createLeadForm_description").sendKeys("Creating profile");
		driver.findElementById("createLeadForm_importantNote").sendKeys("new");
		driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("123");
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("123");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("456");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("www.google.com");
		driver.findElementById("createLeadForm_departmentName").sendKeys("CS");
		WebElement crncy = driver.findElementById("createLeadForm_currencyUomId");
		Select c = new Select(crncy);
		c.selectByVisibleText("IRR - Iranian Rial");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("AB");
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("5");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("ABC");
		driver.findElementById("createLeadForm_generalToName").sendKeys("ABC");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("ADD1");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("ADD2");
		driver.findElementById("createLeadForm_generalCity").sendKeys("chennai");
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("123");
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("456");
		
		WebElement cntry = driver.findElementById("createLeadForm_generalCountryGeoId");
		Select ctry = new Select(cntry);
		ctry.selectByVisibleText("India");
		WebElement state = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
		Select st = new Select(state);
		st.selectByVisibleText("TAMILNADU");
		driver.findElementByName("submitButton").click();
		String text = driver.findElementById("viewLead_firstName_sp").getText();
		if(text.equals(a))
		{
			System.out.println("True");
		}
		else
		{
			System.out.println("False");
		}
		 
	}

}
