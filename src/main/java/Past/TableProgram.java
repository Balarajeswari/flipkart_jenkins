package Past;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TableProgram {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.get("https://erail.in");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MS",Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("MDU",Keys.TAB);
//table level
		WebElement table = driver.findElementByXPath("//Table[@class='DataTable DataTableHeader TrainList']");
		//row level
		List<WebElement> row = table.findElements(By.tagName("tr"));
		System.out.println(row.size());
		WebElement secondRow = row.get(0);
		
		
		//column level
		
		List<WebElement> column = secondRow.findElements(By.tagName("td"));
		System.out.println(column.size());
		String text = column.get(1).getText();
		System.out.println(text);
		
		/*for(int i=0;i<row.size();i++)
		{
			WebElement secondRow = row.get(i);
			List<WebElement> column = secondRow.findElements(By.tagName("td"));
		System.out.println(column.size());
		String text = column.get(1).getText();
		System.out.println(text);
		}*/
		
		
		
		
		
		

	}

}
