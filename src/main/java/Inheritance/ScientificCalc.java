package Inheritance;

public class ScientificCalc extends NormalCalc implements FourthGenCalc {
	public int sub(int a, int b)
	{
		return(a-(-b));
	}

	@Override
	public void touchScreen() {
		System.out.println("Touch Screen Activated");
		
	}

}
