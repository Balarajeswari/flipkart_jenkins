package Inheritance;

public class MyCalc {

	public static void main(String[] args) {
		ScientificCalc sc = new ScientificCalc();
		int addition = sc.add(1, 2, 3);
		int subtraction = sc.sub(3, -2);
		sc.touchScreen();
		System.out.println("Addition value:" + addition);
		System.out.println("Subtraction value:" + subtraction);

	}

}
