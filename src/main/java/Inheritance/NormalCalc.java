package Inheritance;

public class NormalCalc {

	public int add(int a,int b)
	{   
		return (a+b);
	}
	
	public int add(int a,int b,int c)
	{   
		return (a+b+c);
	}
	
	public int sub(int a, int b)
	{
		return(a-b);
	}

	
	
}
